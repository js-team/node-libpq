# Installation
> `npm install --save @types/libpq`

# Summary
This package contains type definitions for libpq (https://github.com/brianc/node-libpq#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/libpq.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 16:23:43 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Vlad Rindevich](https://github.com/Lodin).
